package main

import (
	"testing"
)

func TestAppend(t *testing.T){
  db := new(UglyDB)
  db.Init("test.csv")
	test_output := make(chan string)
	load_request := new(request)
	load_request.command = "Append test-key test-value"
	load_request.output = test_output
	go func(){db.MainLoop()}()
  db.Commands <- *load_request
  test := <- test_output
  if test != "Created new entry"{
    t.Error("error appen/readd")
  }
}

func TestUpdate(t *testing.T){
  db := new(UglyDB)
  db.Init("test.csv")
	test_output := make(chan string)
	load_request := new(request)
	load_request.command = "Append test-key test-value"
	load_request.output = test_output
	go func(){db.MainLoop()}()
  db.Commands <- *load_request
  test := <- test_output
	if test != "Created new entry"{
    t.Error("error appen/readd")
  }
	load_request.command = "Update test-key test-value2"
	go func(){db.MainLoop()}()
	db.Commands <- *load_request
	test = <- test_output
	if test != "Updated successfully"{
    t.Error("error Update")
  }
}

func TestRead(t *testing.T){
	db := new(UglyDB)
  db.Init("test.csv")
	test_output := make(chan string)
	load_request := new(request)
	load_request.command = "Append test-key test-value"
	load_request.output = test_output
	go func(){db.MainLoop()}()
  db.Commands <- *load_request
  test := <- test_output
	if test != "Created new entry"{
    t.Error("error appen/readd")
  }
	load_request.command = "Read test-key"
	go func(){db.MainLoop()}()
	db.Commands <- *load_request
	test = <- test_output
	if test != "test-value"{
    t.Error("error Read")
  }
}

func TestDelete(t *testing.T){
	db := new(UglyDB)
	db.Init("test.csv")
	test_output := make(chan string)
	load_request := new(request)
	load_request.command = "Append test-key test-value"
	load_request.output = test_output
	go func(){db.MainLoop()}()
  db.Commands <- *load_request
  test := <- test_output
	if test != "Created new entry"{
    t.Error("error appen/read")
  }
	load_request.command = "Delete test-key"
	go func(){db.MainLoop()}()
	db.Commands <- *load_request
	test = <- test_output
	if test != "Deleted successfully"{
    t.Error("error Delete")
  }
}
