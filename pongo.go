package main

import (
	"fmt"
	"net"
	"bufio"
	"time"
)

func main(){
	serv, err := net.Listen("tcp", ":135")
	if err != nil {
		fmt.Println("could not bind")
		return
	}

	db := new(UglyDB)
	db.Init("/home/madcake/test.csv")
	db.Load()

	default_output := make(chan string)

	load_request := new(request)
	load_request.command = "Load"
	load_request.output = default_output

	save_request := new(request)
	save_request.command = "Save"
	save_request.output = default_output

	working := true

	go func(){
		for working { db.MainLoop() }
	}()

	db.Commands <- *load_request
	fmt.Println(<- default_output)

	go func(){
	 	for working { pick_clients(serv, db.Commands) }
	}()

	go func(){
		for working {
			db.Commands <- *save_request
			<- default_output
			time.Sleep(time.Second)
		}
	}()

	fmt.Scanln()
	db.Commands <- *save_request
	fmt.Println(<- default_output)

	working = false
}

func pick_clients(serv net.Listener, commands chan request){
	client, err := serv.Accept()
	if err == nil{
		go serve(client, commands)
	}
}

func serve(client net.Conn, commands chan request){
	output := make(chan string)
	for {
		cmd, _ := bufio.NewReader(client).ReadString('\n')

		r := new(request)
		r.command = cmd
		r.output = output

		commands <- *r

		fmt.Fprintln(client, <- output)
	}
}
